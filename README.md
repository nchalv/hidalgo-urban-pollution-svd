  ## SVD calculation code

  This Spark scala code has been developed by ICCS per the requirements of the Hidalgo project.

  ### Prerequisites
  Please make sure *scala* and *sbt* are installed in your system.
  
  ### Build instructions
    cd ./scala
    sbt package

  The generated .jar file can be found under the ``target`` directory.
