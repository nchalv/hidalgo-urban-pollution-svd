/*
 * Run Full/Partial SVD on a set of input matrices.
 *
 * The input is a set of state matrices that are produced from the dispersion
 * module with the ANSYS Fluent solver and contain the saved state variables of
 * the ANSYS Fluent computations at a given save time point as well as some
 * unecessary spatial information.
 *
 * The algorithm for data preprocessing before running SVD is as follows:
 *
 * 1. For each state matrix do:
 *
 *    1.1. Delete all columns that do not correspond to a state variable.
 *         That is the first 4 columns that contain spatial information.
 *
 *    1.2. Delete the header.
 *
 *    1.3. Vectorize the remaining matrix columnwise, i.e., copy the columns
 *         one below the other, from left to right.
 *
 * 2. Compose a matrix from the resulting vectors by copying one state vector
 *    after the other horizontally.
 *
 * 3. Calculate the SVD of the vectorized state matrices produced from Step 2.
 *
 *
 * To do this in Spark we have to do the following:
 *
 * 1. For each state matrix do:
 *
 *    1.1. Read the matrix file into an ml.linalg.DenseMatrix as transposed.
 *
 *    1.2. Take the DenseMatrix as an array and return a Vector[Double].
 *
 * 2. Put all the vectors in an RDD and create a distributed.RowMatrix.
 *
 * 3. Transpose the RowMatrix.
 *
 * 4. Calculate the SVD of the RowMatrix of step 3.
 *
 */

import java.io.File
import org.apache.hadoop.fs.{FileSystem, Path}


import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.mllib.linalg.Matrix
import org.apache.spark.mllib.linalg.DenseMatrix
import org.apache.spark.mllib.linalg.distributed.RowMatrix
import org.apache.spark.mllib.linalg.SingularValueDecomposition
import org.apache.spark.mllib.linalg.Vector
import org.apache.spark.mllib.linalg.Vectors
import Array._
import scala.concurrent.Future
import scala.util.Success
import scala.util.Failure
import scala.concurrent.Await
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global
import java.nio.{ByteBuffer, ByteOrder}


// Configuration for computing SVD.
class Config(
  val inputFiles: Array[String],
  val output: String,
  val computeU: Boolean,
  val singularValues: Int,
  val partitions: Int,
  val appName: String,
  val debugNoTranspose: Boolean,
  val debugTransposeU: Boolean,
  val hdfs: Boolean
)

object SimpleApp {


  def main(args: Array[String]) {
    // Parse command line.
    val config = parseArgs(args)

    val spark = SparkSession
      .builder
      .appName(config.appName)
      .getOrCreate()


    def getffiles(x: String):Array[String]=
	{if (x.endsWith(".csv"))
		Array("hdfs://"+x)
	 else if (x.endsWith(".stx"))
		Array("hdfs://"+x)
	 else
		FileSystem.get(spark.sparkContext.hadoopConfiguration).listStatus(new Path(x)).map("hdfs://"+x+_.getPath.getName)
        }


	def getListOfFiles(dir: String):List[File] = {
	    val d = new File(dir)
	    if (d.exists && d.isDirectory) {
		d.listFiles.filter(_.isFile).toList
	    } else {
		List[File]()
	    }
	}	


    def pipeline_bin(filename: String) = Future[RDD[Vector]] {
      // Open input file.
      var start = System.nanoTime()

      val inputFile = new File(filename)

      val stx1 = spark.sparkContext.binaryRecords(filename, 4)
      val ncols = ByteBuffer.wrap(stx1.first).order(ByteOrder.LITTLE_ENDIAN).getInt
      //val inbetween = stx1.mapPartitionsWithIndex((idx, iter) => if (idx == 0) iter.drop(1) else iter).map(b=>ByteBuffer.wrap(b).order(ByteOrder.LITTLE_ENDIAN).getFloat.toDouble)
      //val intermediate = inbetween.coalesce(1).glom().map((arr: Array[Double]) => {val attr = 2;val points = 5;val rows = arr.length / ncols;new DenseMatrix(rows, ncols, arr, true).toArray.grouped(attr*points).map(f=>Vectors.dense(f)).toList})
      //val candidate = spark.sparkContext.parallelize(intermediate.first)

      stx1
       .mapPartitionsWithIndex((idx, iter) => if (idx == 0) iter.drop(1) else iter)
       .map(b=> ByteBuffer.wrap(b).order(ByteOrder.LITTLE_ENDIAN).getFloat.toDouble)
       .coalesce(1)
       .glom()
       .map((arr: Array[Double]) => {
          val rows = arr.length / ncols
          // Transform RDD to column major vector.
          val mat = new DenseMatrix(rows, ncols, arr, true).toArray
          Vectors.dense(mat.toArray)
        }
      )
    }




    def pipeline(filename: String) = Future[RDD[Vector]] {
      // Open input file.
      var start = System.nanoTime()

      val inputFile = new File(filename)

      spark
        .sparkContext.textFile(filename)
        .mapPartitionsWithIndex(
          (idx, iter) => if (idx == 0) iter.drop(1) else iter
        )
       .flatMap(
//         line => line.split(",").drop(5).map(num => num.toDouble)
         line => line.split(",").drop(3).dropRight(1).map(num => num.toDouble)
       )
       .coalesce(1)
       .glom()
       .map((arr: Array[Double]) => {
          val cols = 7
          val rows = arr.length / cols
          // Transform RDD to column major vector.
          val mat = new DenseMatrix(rows, cols, arr, true).toArray
          Vectors.dense(mat.toArray)
        }
      )
    }

//needs to be transposed(???)

 	
    var inputs = new Array[String](0)
    if (config.hdfs){
	config.inputFiles.foreach(x=>inputs=concat(inputs, getffiles(x)))
    } else {
      inputs = getListOfFiles(config.inputFiles(0).replace("file://","")).toArray.map(x=>x.toString).map(x=>"file://"+x)
    }


//    inputs = Array("file:///home/ubuntu/partitioned_small/snapshot_matrix_000.stx", "file:///home/ubuntu/partitioned_small/snapshot_matrix_001.stx", "file:///home/ubuntu/partitioned_small/snapshot_matrix_002.stx", "file:///home/ubuntu/partitioned_small/snapshot_matrix_003.stx", "file:///home/ubuntu/partitioned_small/snapshot_matrix_004.stx", "file:///home/ubuntu/partitioned_small/snapshot_matrix_005.stx", "file:///home/ubuntu/partitioned_small/snapshot_matrix_006.stx", "file:///home/ubuntu/partitioned_small/snapshot_matrix_007.stx", "file:///home/ubuntu/partitioned_small/snapshot_matrix_008.stx", "file:///home/ubuntu/partitioned_small/snapshot_matrix_009.stx")


    // Schedule a Future for each input file.
    val result = Future.sequence(
      inputs.map(file => pipeline_bin(file)).toList
      //inputs.map(file => pipeline_bin_consolidated(file)).toList
    ) andThen {
      case Success(vectors: List[RDD[Vector]]) => {
        // Create one distributed RowMatrix from all the state matrices.
        var start = System.nanoTime()
        val vectorsRdd: RDD[Vector] = vectors.reduce((u, v) => u.union(v))

//	vectorsRdd.collect.foreach(println)
        println("Partitions originally: " + vectorsRdd.getNumPartitions)

        val rowMat = if (config.partitions != -1 ) {
          println("Repartitioning to: " + config.partitions + " partitions.")

          new RowMatrix(vectorsRdd.repartition(config.partitions))
        } else {
          new RowMatrix(vectorsRdd)
        }

        var end = System.nanoTime()

        println("Create RowMatrix Elapsed Time: " + (end - start) / 1e9 + "." + (end - start) / 1e6 + "s")

        start = System.nanoTime()


        // Transpose the matrix.
        val maybeTransposedRowMat = if (config.debugNoTranspose) {
          rowMat
        } else {
          transposeRowMatrix(rowMat)
        }

        end = System.nanoTime()

        println("Transpose RowMatrix Elapsed Time: " + (end - start) / 1e9 + "." + (end - start) / 1e6 + "s")

        // Compute SVD.
        start = System.nanoTime()

        val singularValues = if (config.singularValues == -1) {
          maybeTransposedRowMat.numCols().toInt
        } else {
          config.singularValues
        }

        val svd = maybeTransposedRowMat.computeSVD(singularValues, computeU = config.computeU)

        end = System.nanoTime()

        println("ComputeSVD Elapsed Time: " + (end - start) / 1e9 + "." + (end - start) / 1e6 + "s")

        start = System.nanoTime()

        // Save output matrices.
        start = System.nanoTime()

        val outputDirectoryU = config.output + "U"
        val outputDirectoryS = config.output + "S"
        val outputDirectoryV = config.output + "V"

        spark
          .sparkContext
          .parallelize(
            svd.V.rowIter.map(line => line.toArray.mkString(",")).toSeq
          )
          .coalesce(1)
          .saveAsTextFile(outputDirectoryV)

        spark
          .sparkContext
          .parallelize(Seq(svd.s.toArray.mkString(",")))
          .coalesce(1)
          .saveAsTextFile(outputDirectoryS)

        end = System.nanoTime()

        println("Save S, V Elapsed Time: " + (end - start) / 1e9 + "." + (end - start) / 1e6 + "s")

        if (config.computeU) {
          val U = if (config.debugTransposeU) {
            start = System.nanoTime()

            val transposedU = transposeRowMatrix(svd.U)

            end = System.nanoTime()

            println("Transpose U Elapsed Time: " + (end - start) / 1e9 + "." + (end - start) / 1e6 + "s")

            transposedU
          } else {
            svd.U
          }

          start = System.nanoTime()

          U.rows
           .map(
              line => line.toArray.mkString(",")
            )
            .repartition(1)
            .saveAsTextFile(outputDirectoryU)

          end = System.nanoTime()

          println("Save U Elapsed Time: " + (end - start) / 1e9 + "." + (end - start) / 1e6 + "s")
        }

      }
      case Failure(ex)  => println("Exception: " + ex.getMessage)
    }

    // Wait for futures to finish. Abort after 60 minutes.
    Await.ready(result, 60.minutes).onComplete {
      case Success(_) => println("Success!")
      case Failure(ex)  => println("Exception: " + ex.getMessage)
    }

    spark.stop()
  }

  // Parses command line arguments.
  def parseArgs(args: Array[String]) : Config = {

    var i = 0
    var flags = true
    var input: Array[String] = Array()
    var output: String = ""
    var computeU: Boolean = false
    var singularValues: Int = -1
    var partitions: Int = -1
    var appName: String = "PartialFullSVD"
    var debugNoTranspose: Boolean = false
    var debugTransposeU: Boolean = false
    var hdfs: Boolean = false

    while (flags && i < args.length-1) {
        if (args(i) == "-i" || args(i) == "--input") {
          if (i + 1 >= args.length) {
            usage()
            sys.exit(2)
          }

          input :+= args(i + 1)

          i += 2
        } else if (args(i) == "-o" || args(i) == "--output") {
          if (i + 1 >= args.length) {
            usage()
            sys.exit(2)
          }

          output = args(i + 1)

          i += 2
        } else if (args(i) == "-u" || args(i) == "--computeU") {
          computeU = true

          i += 1
        } else if (args(i) == "-s" || args(i) == "--singular-values") {
          if (i + 1 >= args.length) {
            usage()
            sys.exit(2)
          }

          singularValues = args(i + 1).toInt

          i += 2
        } else if (args(i) == "-a" || args(i) == "--app-name") {
          if (i + 1 >= args.length) {
            usage()
            sys.exit(2)
          }

          appName = args(i + 1)

          i += 2
        } else if (args(i) == "-p" || args(i) == "--partitions") {
          if (i + 1 >= args.length) {
            usage()
            sys.exit(2)
          }

          partitions = args(i + 1).toInt

          i += 2
        } else if (args(i) == "--debug-no-transpose") {
          debugNoTranspose = true

          i += 1
        } else if (args(i) == "--debug-transpose-u") {
          debugTransposeU = true

          i += 1
        } else if (args(i) == "--hdfs") {
          hdfs = true

          i += 1
        } else {
          flags = false
        }
    }

    if (output == "" || args.length == i) {
      usage()
      System.exit(2)
    }

    val config: Config = new Config(
      input,
      output,
      computeU,
      singularValues,
      partitions,
      appName,
      debugNoTranspose,
      debugTransposeU,
      hdfs
    )

    return config
  }

  // Utilities for transposing a RowMatrix.
  //
  // Taken from:
  //    https://gist.github.com/umbertogriffo/25050693923cd751105fe98443caa156
  def transposeRowMatrix(m: RowMatrix): RowMatrix = {
    val transposedRowsRDD = m.rows.zipWithIndex.map{case (row, rowIndex) => rowToTransposedTriplet(row, rowIndex)}
      .flatMap(x => x) // now we have triplets (newRowIndex, (newColIndex, value))
      .groupByKey
      .sortByKey().map(_._2) // sort rows and remove row indexes
      .map(buildRow) // restore order of elements in each row and remove column indexes
    new RowMatrix(transposedRowsRDD)
  }

  def rowToTransposedTriplet(row: Vector, rowIndex: Long): Array[(Long, (Long, Double))] = {
    val indexedRow = row.toArray.zipWithIndex
    indexedRow.map{case (value, colIndex) => (colIndex.toLong, (rowIndex, value))}
  }

  def buildRow(rowWithIndexes: Iterable[(Long, Double)]): Vector = {
    val resArr = new Array[Double](rowWithIndexes.size)
    rowWithIndexes.foreach{case (index, value) =>
      resArr(index.toInt) = value
    }
    Vectors.dense(resArr)
  }

  // Prints a usage message.
  def usage() {
    val message = """
usage: <program> -o OUTPUT [OPTIONS] <INPUT-FILE> ...

options:
  -o OUTPUT, --output OUTPUT
                        output location.
  -u, --computeU        compute U matrix.
  -s SINGULAR_VALUES, --singular-values SINGULAR_VALUES
                        number of leading singular values to keep.
  -a APP_NAME, --app-name APP_NAME
                        the Spark application name.
  -p PARTITIONS --partitions PARTITIONS
                        number of partitions for each RDD.
  --hdfs                prepend hdfs:// to every input file.
  --debug-no-transpose  do not transpose input matrix.
  --debug-transpose-u   transpose u matrix."""

    println(message)
  }
}
